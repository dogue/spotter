use std::io::{ stdout, Write };
use mpris::PlayerFinder;
use termion::{ clear, color::Rgb, cursor, style, terminal_size, color };
use clap::{ App, Arg };
use std::time::Duration;
use std::thread::sleep;
use serde_derive::Deserialize;
use std::fs::read_to_string;

fn main() {
    let config: Config = toml::from_str(&read_to_string("spotter.toml").unwrap()).unwrap();
    write!(stdout(), "{}", cursor::Hide).unwrap();

    ctrlc::set_handler(move || {
        write!(stdout(), "{}", clear::All).unwrap();
        write!(stdout(), "{}", cursor::Show).unwrap();
        write!(stdout(), "{}", termion::color::Reset.fg_str()).unwrap();
        write!(stdout(), "{}", termion::color::Reset.bg_str()).unwrap();
        write!(stdout(), "{}", style::Reset).unwrap();
        stdout().flush().unwrap();

        std::process::exit(0);

    }).unwrap();

    loop {
        let mut track = get_track_info();
        draw_info(&mut track, &config).unwrap();
        
        sleep(Duration::from_millis(1000));
    }
}

fn get_rgb(color: &Vec<u8>) -> (u8, u8, u8) {
    match &color[..] {
        &[r, g, b] => (r, g, b),
        _ => unreachable!()
    }
}

fn get_style(style: &str) -> String {
    match &style[..] {
        "bold" => style::Bold.to_string(),
        //"faint" => style::Faint.to_string(),
        "italic" => style::Italic.to_string(),
        "underline" => style::Underline.to_string(),
        _ => style::Reset.to_string()
    }
}

fn draw_info(mut track: &mut Track, config: &Config) -> Result<(), std::io::Error> {
    write!(stdout(), "{}", clear::All)?;

    let cursors = calc_cursors(&mut track, &config.offset_y);

    let (r, g, b) = get_rgb(&config.title.fg_color);
    let style = get_style(&config.title.style);
    write!(stdout(), "{}{}{}", style, Rgb(r, g, b).fg_string(), cursor::Goto(cursors.title, cursors.term_y))?;
    write!(stdout(), "{}", track.title)?;
    write!(stdout(), "{}{}{}", color::Reset.fg_str(), color::Reset.bg_str(), style::Reset).unwrap();

    let (r, g, b) = get_rgb(&config.artist.fg_color);
    let style = get_style(&config.artist.style);
    write!(stdout(), "{}{}{}", style, Rgb(r, g, b).fg_string(), cursor::Goto(cursors.artist, cursors.term_y + 1))?;
    write!(stdout(), "{}", track.artist)?;
    write!(stdout(), "{}{}{}", color::Reset.fg_str(), color::Reset.bg_str(), style::Reset).unwrap();

    let (r, g, b) = get_rgb(&config.album.fg_color);
    let style = get_style(&config.album.style);
    write!(stdout(), "{}{}{}", style, Rgb(r, g, b).fg_string(), cursor::Goto(cursors.album, cursors.term_y + 2))?;
    write!(stdout(), "{}", track.album)?;
    write!(stdout(), "{}{}{}", color::Reset.fg_str(), color::Reset.bg_str(), style::Reset).unwrap();

    stdout().flush().unwrap();
    Ok(())
}

fn calc_cursors(track: &mut Track, offset: &u16) -> Cursors {
    let (x, y) = terminal_size().unwrap();
    Cursors {
        term_y: (y / 2)  - (y % 2) + offset,
        title: (x / 2) - (track.title.len() as u16 / 2),
        artist: (x / 2) - (track.artist.len() as u16 / 2),
        album: (x / 2) - (track.album.len() as u16 / 2)
    }
}

#[derive(Deserialize)]
struct Config {
    title: Style,
    artist: Style,
    album: Style,
    offset_y: u16
}

#[derive(Deserialize)]
struct Style {
    bg_color: Vec<u8>,
    fg_color: Vec<u8>,
    style: String
}

struct Cursors {
    title: u16,
    artist: u16,
    album: u16,
    term_y: u16
}

struct Track {
    title: String,
    artist: String,
    album: String,
}

fn get_spotify() -> Option<mpris::Player<'static>> {
    let players = PlayerFinder::new().unwrap().find_all().unwrap();
    for app in players {
        match app.identity() {
            "Spotify" => return Some(app),
            _ => {}
        }
    }
    None
}

fn get_track_info() -> Track {
    let player = get_spotify().unwrap();
    
    let metadata = player.get_metadata().expect("Could not get metadata");
    let artist = match metadata.artists() {
        Some(artist) => &artist[0],
        None => "Unknown Artist"
    };

    let album = match metadata.album_name() {
        Some(album) => &album,
        None => "Unknown Album"
    };

    let title = match metadata.title() {
        Some(title) => &title,
        None => "Unknown Track"
    };

    Track {
        title: String::from(title),
        artist: String::from(artist),
        album: String::from(album),
    }
}

#[test]
fn test_track() {
    let track = get_track_info();

    assert_eq!(track.title, "Fragments");
}